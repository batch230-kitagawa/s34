// Load the expressjs module into our application and saved it in variable called express

const express = require("express");
const port = 3000;

// app is our server
// create an application that uses express and stores it as app
const app = express();

// Middleware (request handlers)
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

// mock data
let users = [
{
	username: "TStark3000",
	email: "starkindustries@mail.com",
	password: "notPeterParker"
},
{
	username: "ThorThunder",
	email: "thorStrongestAvenger@mail.com",
	password: "iLoveStormBreaker"
}]

/*
let items = [
{
	name: "Mjolnir",
	price: 50000,
	isActive: true
},
{
	name: "Vibranium Shield",
	price: 70000,
	isActive: true
}]
*/
app.get("/greeting", (request, response) => {
	response.send("Hello from Batch230-kitagawa");
	}
	)

// retrieval of users in mock database
app.get("/users", (req, res) => {
	res.send(users);
})


app.post("/users", (request,response) =>{
	let newUser = {
		username: request.body.username,
		email: request.body.email,
		password: request.body.password
	}

	users.push(newUser);
	console.log(users);

	response.send(users);
})

app.put("/users/:index", (req,res) => {
	console.log(req.body);

	console.log(req.params);
	let index = parseInt(req.params.index);

	users[index].password = req.body.password;

	res.send(users[index]);
})

// Delete the last element in an array
app.delete("/users", (req,res) => {
	users.pop();
	res.send(users);
})

//////////////////////////////////////
let items = [
{
	name: "Mjolnir",
	price: 50000,
	isActive: true
},
{
	name: "Vibranium Shield",
	price: 70000,
	isActive: true
}]

// Activity
/*
    >> Create a new collection in Postman called s34-activity
    >> Save the Postman collection in your s34 folder
*/

// [GET]
// >> Create a new route to get and send items array in the client (GET ALL ITEMS)
// Insert your code here...

	app.get("/items", (req, res) => {
	res.send(items);
})

// [POST]
// >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
    // >> send the updated items array in the client
    // >> check if the post method route for our users for reference

    app.post("/items", (request,response) =>{
	let newItem = {
		name: request.body.name,
		price: request.body.price,
		isActive: request.body.isActive
	}

	items.push(newItem);
	console.log(items);

	response.send(items);
})


// [PUT]
// >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
/*
    >> Pass the index number of the item that you want to update in the request params (include a index number to the URL)
    >> add the price update update in the request body
    >> reassign the new price from our request body
    >> send the updated item to the client
*/

	app.put("/items/:index", (req,res) => {
	console.log(req.body);

	console.log(req.params);
	let index = parseInt(req.params.index);

	items[index].price = req.body.price;

	res.send(items[index]);
})


app.listen(port, () => console.log(`Server is running at port ${port}`));